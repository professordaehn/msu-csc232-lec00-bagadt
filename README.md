# Bag ADT
The first abstract data type we explore is a container of things, or as most laypersons might say, a *bag*.

## Bag CRC Card
Appendix C discusses a common methods used to capture the requirements of some abstract data type, for example, the **class-responsibility-collaboration (CRC) card**. Below is an example CRC used to capture the behaviors of the *Bag* ADT.

**Responsibilities**

* Get the number of items currently in the bag
* See whether the bag is empty
* Add a given object to the bag
* Remove an occurrence of a specific object from the bag, if possible
* Count the number of times a certain object occurs in the bag
* Test whether the bag contains a particular object
* Look at all objects that are in the bag

**Collaborations**

* The class of objects that the bag can contain